<?php
//Template Name: Template-Especialidades
get_header();
?>

<section id="page-especialidades">
	<div class="top-main-title">
		<?php 
		if(wp_is_mobile()){
			$url = wp_get_attachment_image_url( the_post_thumbnail(), 'mobile-post'); 
		}else{
			$url = wp_get_attachment_image_url( the_post_thumbnail(), 'full-post');
		}
		?>
		<div class="container">
			<div class="title-post">
				<h1>Nossas<p>Especialidades</p></h1>
			</div>
		</div>
	</div>
	<div class="bg-special">
		<div class="container">
			<div class="col">
				<div class="breadcrumb">
					<span class="line-purple bar-page"></span>	
					<?php get_breadcrumb(); ?>
				</div>
				<?php
			// TO SHOW THE PAGE CONTENTS
				while ( have_posts() ) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
				<div class="desc-post">
					<?php the_content(); ?> <!-- Page Content -->
				</div>
				<?php
				    endwhile; //resetting the page loop
				    wp_reset_query(); //resetting the page query
				    ?>
				    
				    <div class="boxes-special">

				    	<div class="col-sm box-special">
				    		<div class="line-box"></div>
				    		<div class="special-item">
				    			<div class="border-image">
				    				<img class="img-aside" src="<?php echo get_template_directory_uri(). '/img/especialidades/ginecologia.jpg' ?>" alt="Ginecologia" loading="lazy">
				    			</div>
				    			<div class="text-box">
				    				<div class="title-box">
				    					<p><strong>Ginecologia</strong></p>

				    				</div>

				    				<p>O ginecologista é o responsável por tratar problemas como cólicas menstruais, doenças sexualmente transmissíveis, endometriose e outras. </p>
				    			</div>
				    		</div>
				    	</div>

				    	<div class="col-sm box-special">
				    		<div class="line-box"></div>
				    		<div class="special-item">
				    			<div class="border-image">
				    				<img class="img-aside" src="<?php echo get_template_directory_uri(). '/img/especialidades/obstetricia.jpg' ?>" alt="Obstetrícia" loading="lazy">
				    			</div>
				    			<div class="text-box">
				    				<div class="title-box">
				    					<p><strong>Obstetrícia</strong></p>

				    				</div>

				    				<p>O obstetra é o médico especialista em acompanhar o desenvolvimento do feto, além de prestar assistência à mulher, desde o pré-natal até o parto e o pós-parto.</p>
				    			</div>
				    		</div>
				    	</div>

				    	<div class="col-sm box-special">
				    		<div class="line-box"></div>
				    		<div class="special-item">
				    			<div class="border-image">
				    				<img class="img-aside" src="<?php echo get_template_directory_uri(). '/img/especialidades/cardiologia.jpg' ?>" alt="Cardiologia" loading="lazy">
				    			</div>
				    			<div class="text-box">
				    				<div class="title-box">
				    					<p><strong>Cardiologia</strong></p>

				    				</div>

				    				<p>A cardiologia é considerada como uma das especializações da área da medicina mais evoluídas, por conta dos grandes investimentos e tecnologias envolvidas. </p>
				    			</div>
				    		</div>
				    	</div>

				    	<div class="col-sm box-special">
				    		<div class="line-box"></div>
				    		<div class="special-item">
				    			<div class="border-image">
				    				<img class="img-aside" src="<?php echo get_template_directory_uri(). '/img/especialidades/clinica-medica.jpg' ?>" alt="Clínica Médica" loading="lazy">
				    			</div>
				    			<div class="text-box">
				    				<div class="title-box">
				    					<p><strong>Clínica Médica</strong></p>

				    				</div>

				    				<p>A Clínica Médica é uma área de especialização da Medicina que cuida da prevenção, diagnóstico e tratamento clínico de doenças.</p>
				    			</div>
				    		</div>
				    	</div>

				    	<div class="col-sm box-special">
				    		<div class="line-box"></div>
				    		<div class="special-item">
				    			<div class="border-image">
				    				<img class="img-aside" src="<?php echo get_template_directory_uri(). '/img/especialidades/medicina-fetal.jpg' ?>" alt="Medicina Fetal" loading="lazy">
				    			</div>
				    			<div class="text-box">
				    				<div class="title-box">
				    					<p><strong>Medicina Fetal</strong></p>

				    				</div>

				    				<p>A Medicina Fetal é considerada uma subespecialidade da Obstetrícia dedicada a dar uma maior assistência ao feto, acompanhando todo o desenvolvimento fetal.</p>
				    			</div>
				    		</div>
				    	</div>

				    	<div class="col-sm box-special">
				    		<div class="line-box"></div>
				    		<div class="special-item">
				    			<div class="border-image">
				    				<img class="img-aside" src="<?php echo get_template_directory_uri(). '/img/especialidades/ecocardiograma-fetal.jpg' ?>" alt="Ecocardiograma Fetal" loading="lazy">
				    			</div>
				    			<div class="text-box">
				    				<div class="title-box">
				    					<p><strong>Ecocardiograma Fetal</strong></p>

				    				</div>

				    				<p>O Ecocardiograma Fetal é um exame responsável por avaliar, em tempo real, algumas características do coração do feto durante a gravidez.</p>
				    			</div>
				    		</div>
				    	</div>

				    	<div class="col-sm box-special">
				    		<div class="line-box"></div>
				    		<div class="special-item">
				    			<div class="border-image">
				    				<img class="img-aside" src="<?php echo get_template_directory_uri(). '/img/especialidades/cardiotocografia.jpg' ?>" alt="Cardiotocografia" loading="lazy">
				    			</div>
				    			<div class="text-box">
				    				<div class="title-box">
				    					<p><strong>Cardiotocografia</strong></p>

				    				</div>

				    				<p>A cardiotocografia (CTG) é um exame realizado durante a gravidez para avaliar o bem-estar fetal. Esse exame pode ser indicado após as 37 semanas de gestação.</p>
				    			</div>
				    		</div>
				    	</div>

				    	<div class="col-sm box-special">
				    		<div class="line-box"></div>
				    		<div class="special-item">
				    			<div class="border-image">
				    				<img class="img-aside" src="<?php echo get_template_directory_uri(). '/img/especialidades/eletrocardiograma.jpg' ?>" alt="Eletrocardiograma" loading="lazy">
				    			</div>
				    			<div class="text-box">
				    				<div class="title-box">
				    					<p><strong>Eletrocardiograma</strong></p>

				    				</div>

				    				<p>O eletrocardiograma é um exame que observa o ritmo, a velocidade e a quantidade das batidas do coração.</p>
				    			</div>
				    		</div>
				    	</div>

				    	<div class="col-sm box-special">
				    		<div class="line-box"></div>
				    		<div class="special-item">
				    			<div class="border-image">
				    				<img class="img-aside" src="<?php echo get_template_directory_uri(). '/img/especialidades/uroginecologia.jpg' ?>" alt="Uroginecologia" loading="lazy">
				    			</div>
				    			<div class="text-box">
				    				<div class="title-box">
				    					<p><strong>Uroginecologia</strong></p>

				    				</div>

				    				<p>A uroginecologia é uma subespecialidade da ginecologia e da urologia que abrange o tratamento do sistema urinário feminino e suas estruturas adjacentes.</p>
				    			</div>
				    		</div>
				    	</div>
				    </div>
				    <p class="text-swipe"><strong><< </strong> Deslize para ver mais. <strong> >></strong></p>
				</div>
			</div>
		</div>
	</section>

	<?php get_template_part( 'includes/components/form-ajuda'); ?>
	<?php get_template_part('includes/components/contact') ?>

	
	<?php get_footer(); ?>