<?php
//Template Name: Template-Hospital
get_header();
?>

<section id="page-hospital">
	<div class="top-main-title">
			<?php 
		if(wp_is_mobile()){
			$url = wp_get_attachment_image_url( the_post_thumbnail(), 'mobile-post'); 
		}else{
			$url = wp_get_attachment_image_url( the_post_thumbnail(), 'full-post');
		}
		?>
	
		<div class="container">
			<div class="title-post">
				<h1><p>Hospitais</p></h1>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="col">
			<div class="breadcrumb">
				<span class="line-purple bar-page"></span>	
				<?php get_breadcrumb(); ?>
			</div>
			<p class="desc-post text-hospital">O Centro Médico Alphaville só realiza seus procedimentos cirúrgicos em hospitais reconhecidos por oferecer todo suporte para se obter o máximo em segurança. Seja para partos de baixo ou alto risco, assim como cirurgias pequenas ou complexas, é no centro cirúrgico de hospitais renomados que os pacientes recebem as orientações necessárias, realizam os procedimentos pré-cirúrgicos e passam por cirurgia.</p>

		</div>
	</div>
	<div class="slider-hospital">
		
		<!-- horizontal bar -->
		<div class="detail-column hospital">				
		</div>
		<!-- end -->
		<div class="container">
			<div class="title-slider">
				<h2 class="title-default">Hospitais de Excelência</h2>
				<span class="line-purple light-line"></span>

				<div class="content-slider">
					<p class="white-text">Os profissionais da equipe médica do Centro Médico Alphaville também fazem parte do corpo clínico dos grandes hospitais da região, como por exemplo:</p>

					<div class="hospitals">
						<div class="slides-hospital">
							<div class="item-hospital">
								<div class="img-hospital">
									<img src="<?php echo get_template_directory_uri(). '/img/img-slide-hospital.jpg'; ?>" loading="lazy">
								</div>
								<p class="desc-slide-hospital"><strong>R.</strong> Francisco Marengo, 1312 | <strong>Jardim Anália Franco São Paulo</strong>/SP | <strong>03313-001</strong></p>
							</div>	
							<div class="item-hospital">
								<div class="img-hospital">
									<img src="<?php echo get_template_directory_uri(). '/img/img-slide-hospital.jpg'; ?>" loading="lazy">
								</div>
								<p class="desc-slide-hospital"><strong>R.</strong> Francisco Marengo, 1312 | <strong>Jardim Anália Franco São Paulo</strong>/SP | <strong>03313-001</strong></p>
							</div>
							<div class="item-hospital">
								<div class="img-hospital">
									<img src="<?php echo get_template_directory_uri(). '/img/img-slide-hospital.jpg'; ?>" loading="lazy">
								</div>
								<p class="desc-slide-hospital"><strong>R.</strong> Francisco Marengo, 1312 | <strong>Jardim Anália Franco São Paulo</strong>/SP | <strong>03313-001</strong></p>
							</div>
							<div class="item-hospital">
								<div class="img-hospital">
									<img src="<?php echo get_template_directory_uri(). '/img/img-slide-hospital.jpg'; ?>" loading="lazy">
								</div>
								<p class="desc-slide-hospital"><strong>R.</strong> Francisco Marengo, 1312 | <strong>Jardim Anália Franco São Paulo</strong>/SP | <strong>03313-001</strong></p>
							</div>
						</div>
						<p class="text-swipe swipe-white"><strong><<</strong> Deslize para ver mais. <strong>>></strong></p>	
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="desc-hospital">
		<h3 class="subtitle-hospital">Agende uma consulta agora mesmo!</h3>
		<p class="text-hospital">O corpo clínico do Centro Médico Alphille é composto por profissionais que são referência em suas especialidades e estão à disposição para prestar um serviço humanizado para cuidar da sua saúde. Aliados às instalações e equipamentos de ponta de alguns dos maiores hospitais do país, os médicos proporcionam um atendimento de qualidade em partos ou casos de pacientes que precisem de tratamento cirúrgico.</p>
	</div>
</div>


<?php get_template_part('includes/components/contact') ?>

</section>
<?php get_footer(); ?>
