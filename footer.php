<footer>
	<div class="bg-footer">
		<div class="items-footer">
			<div class="container">

				<div class="col-sm">
					<a href="<?php echo home_url(); ?>">
						<img src="<?php echo get_template_directory_uri(). '/img/logo-footer.png' ?>" alt="Logotipo Centro Médico Alphaville" loading="lazy">
					</a>
					<p class="text-footer">A combinação de uma nova instalação no centro de Alphaville no Edifício Alpha Premium, médicos especializados por instituições de ponta, serviços em medicina diagnóstica, somadas aos 24 anos de experiência da clinica, garantem que qualquer atendimento no Centro Médico Alphaville corresponda a sua expectativa.</p>

				</div>
				<div class="col-sm">
					<div class="topics-footer">
						<?php 
							wp_nav_menu([
								'theme_location' => 'footer-menu-main',
								'menu_class' => 'strong-text',
								'container_class' => 'footer-menu-main',
							]);
						?>					
					</div>
				</div>
				<div class="col-sm">
					<div class="topics-footer">
						<p class="strong-text">Especialidades</p>
					
						<?php
						wp_nav_menu([
							'theme_location' => 'footer-especial',
							'menu_class' => 'list-items-footer',
							'container_class' => 'footer-topics-1',
						]);
						?>
					</div>
				</div>
				<div class="col-sm">
					<div class="topics-footer">
						<p class="strong-text">Convênios</p>

						<?php
						wp_nav_menu([
							'theme_location' => 'footer-convenios',
							'menu_class' => 'list-items-footer',
							'container_class' => 'footer-topics-2',
						]);
						?>
					</div>
				</div>
				<div class="col-sm">
					<div class="topics-footer">
						<p class="strong-text">Newsletter</p>
						<p class="text-footer news-text">Quer receber conteúdo especializado em procedimentos clínicos? Envie seu e-mail e receba nossa newsletter.</p>
						<form data-modal="form-news">
							<input type="hidden" name="formulario" value="E-mail para Newsletter">
							<input class="input-news" type="email" placeholder="Seu melhor E-mail" required>
							<button class="btn-news">
								Enviar<img src="<?php echo get_template_directory_uri(). '/img/icons/icon-plane-footer.png' ?>" alt="Enviar formulário" loading="lazy"></button>
							</form>
						</div>

						<div class="topics-footer">
							<p class="strong-text">Redes Sociais</p>
							<a href="">
							<img src="<?php echo get_template_directory_uri(). '/img/icons/icon-facebook-footer.png' ?>" alt="Ícone Facebook">
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="copyright">
			<div class="container">
				<p class="text-copyright"><strong>Centro Médico Alphaville. | </strong> Copyright © Todos os direitos Reservados.</p>
				<a target="_blank" href="https://www.3xceler.com.br/marketing-para-medicos/">
				<p class="logo-copyright">Marketing para Médicos:<img src="<?php echo get_template_directory_uri(). '/img/icons/icon-logo-3xceler.png'; ?>" loading="lazy"></p>
				</a>
			</div>
		</div>	
	</footer>
	<?php 
get_template_part('includes/components/mobile-cta-wrapper');
get_template_part('includes/components/cta-wrapper');
	
	get_template_part('includes/components/oldie'); 
	wp_footer();
	?>

</body>
</html> 
