
  $.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

$(document).ready(function () {
    var templateUrl = object_name.templateUrl;
    var origin = window.location.href;

    $('body').on('submit', 'form', function (event) {
        event.preventDefault();

            // set generic variables
            var fields = $(this).serializeObject(),
             mailInstance,
             instance = $(this).data('modal'),
            redirect; 

            // sort form instance
            switch (instance) {

                // Form-Podemos-te-ajudar
                case 'form-podemos-te-ajudar':
                mailInstance = templateUrl + '/includes/forms-submit.php';
                redirect = origin + 'obrigado-ajuda/';
                break;

                // Form-Contato
                case 'form-contato':
                mailInstance    = templateUrl + '/includes/forms-submit.php';
                redirect        = origin + 'obrigado-contato/';
                break;

                // Mail newsletter
                case 'form-news':
                mailInstance = templateUrl + '/includes/forms-submit.php';
                redirect = origin + 'obrigado-newsletter/';
                break;    


                  // Nós te ligamos
                case 'box-ligar-agora':
                mailInstance = templateUrl + '/includes/forms-submit.php';
                redirect = origin + 'obrigado-ligar-agora/';
                break;

                // Agendar horário
                case 'box-ligar-depois':
                mailInstance    = templateUrl + '/includes/forms-submit.php';
                redirect        = origin + 'obrigado-ligar-depois/';
                break;

                // Deixar uma mensagem
                case 'box-deixar-mensagem':
                mailInstance = templateUrl + '/includes/forms-submit.php';
                redirect = origin + 'obrigado-deixar-mensagem/';
                break;    


                default:
                return false;
            }

            // ajax to email submit
            $.ajax({
                url: mailInstance,
                type: 'POST',
                data: fields,
            })
            .done(function (r) {
                swal({
                    title: 'Obrigado pelo Contato!',
                    text: 'Nossos Analistas irão responder o mais cedo possível!',
                    icon: 'success',
                });
                setTimeout(function () {
                    window.location =  redirect;
                }, 800);
            })
            .fail(function () {
                swal({
                    title: 'Algo deu errado...',
                    text: 'Tente novamente mais tarde ou entre em contato através de nossos telefones.',
                    icon: 'error',
                })
            })
            .always(function () {
                // console.log(fields); 
            });
        });
});




