// unique scripts go here.
function sliderItems(){

	$('.slider').slick({
		arrows:         false,
		fade:           true,
		dots:           true,
		pauseOnFocus:   false,
		pauseOnHover:   false,
		slidesToShow:   1,
		slidesToScroll: 1

	});

	$('.special-items').slick({
		arrows:         true,
		prevArrow:     $('.slide-left'),
		nextArrow:     $('.slide-right'), 
		slidesToShow:   4,
		slidesToScroll: 1,

			responsive:[{
			breakpoint:600,
			settings:{
				slidesToShow: 1,
				slidesToScroll: 1,
	
				fade:true,
			}
		}]
	});

	$('.container-slides').slick({
		arrows:         true,
		prevArrow:      $('.slide-left-slides'),
		nextArrow:      $('.slide-right-slides'),
		pauseOnFocus:   false,
		pauseOnHover:   false,
		slidesToShow:   5,
		slidesToScroll: 1,

		responsive:[{
			breakpoint:600,
			settings:{
				arrows:false,
				fade:true,
				slidesToShow: 1,
				slidesToScroll:1,
			}
		}]
	});

	$('.slider-medic-center').slick({

		arrows:         true,
		fade:           true, 
		prevArrow:     $('.btn-left-alphaville'),
		nextArrow:     $('.btn-right-alphaville'), 
		slidesToShow:   1,
		slidesToScroll: 1,
		responsive:[{
			breakpoint:600,
			settings:{
				slidesToShow:1,
				sliderToScroll:1,
				arrows:false,
				dots:false,
		
			}
		}]
	});

	$('.slides-hospital').slick({
		arrows: true,
		centerMode: true,
		slidesToShow:   3,
		slidesToScroll: 1,
		responsive:[{

			breakpoint:600,
			settings:{
				slidesToShow:1,
				sliderToScroll:1,
				arrows:false,
				dots:false,
		
			}
		}]
	});

	$('.boxes-special').slick({
		mobileFirst: true,
		arrows:false,
		dots:false,
		responsive:[{
			breakpoint:600,
			settings:{
				slidesToShow:1,
				sliderToScroll:1,
	
			},
			breakpoint:1200,
			settings:"unslick"
		}]
	})

	$('.box-images').slick({
		mobileFirst: true,
		arrows:false,
		dots:false,
		responsive:[{
			breakpoint:600,
			settings:{
				slidesToShow:1,
				sliderToScroll:1,
		
			},
			breakpoint:1200,
			settings:"unslick"
		}]
	})

		$('.images-thumbs').slick({
		mobileFirst: true,
		arrows:false,
		dots:false,
		responsive:[{
			breakpoint:600,
			settings:{
				slidesToShow:1,
				sliderToScroll:1,
		
			},
			breakpoint:1200,
			settings:"unslick"
		}]
	})

	$('.boxes-team').slick({
		mobileFirst: true,
		arrows:false,
		dots:false,
		responsive:[{
			breakpoint:600,
			settings:{
				slidesToShow:1,
				sliderToScroll:1,
			},
			breakpoint:1200,
			settings:"unslick"
		}]
	})
}

function optionsMenuLoader(){
	$( "li.menu-item" ).hover(function() {  // mouse enter
	    $( this ).find( " > .sub-menu" ).show(); // display immediate child

	}, function(){ // mouse leave
	    if ( !$(this).hasClass("current_page_item") ) {  // check if current page
	        $( this ).find( ".sub-menu" ).hide(); // hide if not current page
	    }
	});
}

function menuFixed(){
$(window).bind('scroll', function () {
    if ($(window).scrollTop() > 50) {
        $('.top-items').addClass('fixed', 500);
    } else {
        $('.top-items').removeClass('fixed', 500);
    }
});
}

function loadForm(){
	$('.btn-show-contact').click(function(){
		$('.btn-show-contact').fadeOut();
		$('.fields-help').fadeIn().css('display', 'inline-block');
	});
}

function menuMobile(){
	$('.icon-menu-mobile').click(function(){
		$('body, html').css('overflow-y', 'hidden');

		$('.icon-menu-mobile').hide();
		$('.cover-menu').fadeIn();
		$('.sub-menu').addClass('menu-inactive');
		$('.icon-close-mobile').fadeIn();
		$('.outside-menu').fadeIn();	

		$('.mobile-cta-wrapper').fadeOut();
	});

	$('.menu-item-231').click(function(){
		$('.sub-menu').removeClass('menu-inactive');
		$('.sub-menu').addClass('menu-active');
		$('.items-menu-mobile').addClass('menu-inactive');
	});

	$('.menu-item-110').click(function(){
		$('.items-menu-mobile').removeClass('menu-inactive');
		$('.sub-menu').removeClass('menu-active');
		$('.submenu').addClass('menu-inactive');
	});

	$('.icon-close-mobile').click(function(){
		$('.outside-menu').fadeOut();
		$('.icon-close-mobile').hide();
		$('.icon-menu-mobile').fadeIn();
		$('.cover-menu').fadeOut();
		$('.sub-menu').addClass('menu-inactive');
		$('.items-menu-mobile').removeClass('menu-inactive');
		$('.mobile-cta-wrapper').fadeIn();
		$('body, html').css('overflow-y', 'initial');

	});

}


function accordionHandling() {
	var parent = $(this).parents('.accordion-instance');
	var question = parent.children('.accordion-answer');
	var question2 = parent.children('.accordion-question');

	if ( parent.hasClass('is-open') ) {
		
		parent.removeClass('is-open'); 
		question2.removeClass('pressed');

	} else {

		parent.addClass('is-open');
		question2.addClass('pressed');
	}

	question.slideToggle();
}

function loadMask(){
	$('input[name="telefone"]').mask('(00) 00000-0000');
	$('input[name="hora"]').mask('00h00');
	$('input[name="data"]').mask('00/00/0000');	
}