<?php get_header(); ?>
<main class="structure">
	<section id="main-slider">
		<div class="row">
			<div class="bg-top"></div>
			<div class="slider">
				<div class="slide-item">
					<div class="col-2">
						<!-- horizontal bar -->
						<div class="detail-column">							
						</div>
						<!-- end -->
						<div class="main-text">
							<h1 class="title-main">DIU Hormonal:<p class="subtitle-main">MétodoIntrauterino</p></h1>
							<span class="line-purple"></span>
							<p class="desc-main">O DIU Mirena, é um dispositivo pequeno, macio e no formato de T, sendo um dos mais eficientes métodos contraceptivos para mulheres. O dispositivo consiste em um reservatório que contém progesterona e que libera uma dose baixa e contínua desse hormônio no útero.</p>

							<p class="desc-main">O dispositivo intrauterino hormonal é indicado para mulheres que desejam evitar uma gravidez indesejada com um método de alta eficácia de um modo prático.</p>
							<button class="btn-purple">Saiba Mais</button>
						</div>
					</div>
					<img class="img-text" src="<?php echo get_template_directory_uri(). '/img/img-slide1.png' ?>" alt="DIU Hormonal" loading="lazy">
				</div>
				<div class="slide-item">
					<div class="col-2">
						<!-- horizontal bar -->
						<div class="detail-column">							
						</div>
						<div class="main-text">
							<h1 class="title-main">DIU Hormonal:<p class="subtitle-main">MétodoIntrauterino</p></h1>
							<span class="line-purple"></span>
							<p class="desc-main">O DIU Mirena, é um dispositivo pequeno, macio e no formato de T, sendo um dos mais eficientes métodos contraceptivos para mulheres. O dispositivo consiste em um reservatório que contém progesterona e que libera uma dose baixa e contínua desse hormônio no útero.</p>

							<p class="desc-main">O dispositivo intrauterino hormonal é indicado para mulheres que desejam evitar uma gravidez indesejada com um método de alta eficácia de um modo prático.</p>
							<button class="btn-purple">Saiba Mais</button>
						</div>
					</div>
					<img class="img-text" src="<?php echo get_template_directory_uri(). '/img/img-slide1.png' ?>" alt="DIU Hormonal" loading="lazy">
				</div>
				<div class="slide-item">
					<div class="col-2">
						<!-- horizontal bar -->
						<div class="detail-column">						
						</div>
						<div class="main-text">
							<h1 class="title-main">DIU Hormonal:<p class="subtitle-main">MétodoIntrauterino</p></h1>
							<span class="line-purple"></span>
							<p class="desc-main">O DIU Mirena, é um dispositivo pequeno, macio e no formato de T, sendo um dos mais eficientes métodos contraceptivos para mulheres. O dispositivo consiste em um reservatório que contém progesterona e que libera uma dose baixa e contínua desse hormônio no útero.</p>

							<p class="desc-main">O dispositivo intrauterino hormonal é indicado para mulheres que desejam evitar uma gravidez indesejada com um método de alta eficácia de um modo prático.</p>
							<button class="btn-purple">Saiba Mais</button>
						</div>
					</div>
					<img class="img-text" src="<?php echo get_template_directory_uri(). '/img/img-slide1.png' ?>" alt="DIU Hormonal" loading="lazy">
				</div>
				
			</div>
			<div class="detail-background"></div>
			<div class="row">
				<!-- horizontal bar -->
				<div class="detail-column sub">					
				</div>
				<!-- end -->
				<div class="container">
					<div class="col">
						<div class="sub-text">
							<h2 class="subtitle-main">Atendemos os <p class="title-main">Principais Convênios</p></h2>
							<span class="line-purple"></span>
							<p class="desc-full">Além de atendermos pacientes particulares, o Centro Médico Alphaville possui parceria com os principais convênios do Brasil. </p>
							
							<div class="container-slides">
								
								<div class="item-slide">
									<img src="<?php echo get_template_directory_uri(). '/img/convenios/amil.png' ?>" alt="Convênio Amil" loading="lazy" >
								</div>
								<div class="item-slide">
									<img src="<?php echo get_template_directory_uri(). '/img/convenios/bradesco.png' ?>" alt="Convênio Bradesco" loading="lazy" >
								</div>

								<div class="item-slide">
									<img src="<?php echo get_template_directory_uri(). '/img/convenios/cassi.png' ?>" alt="Convênio Cassi" loading="lazy" >
								</div>

								<div class="item-slide">
									<img src="<?php echo get_template_directory_uri(). '/img/convenios/gama.png' ?>" alt="Convênio Gama Saúde" loading="lazy" >
								</div>
								
								<div class="item-slide">
									<img src="<?php echo get_template_directory_uri(). '/img/convenios/golden-cross.png' ?>" alt="Convênio Golden Cross" loading="lazy" >
								</div>

								<div class="item-slide">
									<img src="<?php echo get_template_directory_uri(). '/img/convenios/mapfre.png' ?>" alt="Convênio Mapfre" loading="lazy" >
								</div>

								<div class="item-slide">
									<img src="<?php echo get_template_directory_uri(). '/img/convenios/maritima.png' ?>" alt="Convênio Marítima" loading="lazy" >
								</div>

								<div class="item-slide">
									<img src="<?php echo get_template_directory_uri(). '/img/convenios/medial.png' ?>" alt="Convênio Medial" loading="lazy" >
								</div>

								<div class="item-slide">
									<img src="<?php echo get_template_directory_uri(). '/img/convenios/mediservice.png' ?>" alt="Convênio Service" loading="lazy" >
								</div>

								<div class="item-slide">
									<img src="<?php echo get_template_directory_uri(). '/img/convenios/omint.png' ?>" alt="Convênio Omint" loading="lazy" >
								</div>
								<div class="item-slide">
									<img src="<?php echo get_template_directory_uri(). '/img/convenios/sulamerica.png' ?>" alt="Convênio Sul América" loading="lazy" >
								</div>
								<div class="item-slide">
									<img src="<?php echo get_template_directory_uri(). '/img/convenios/unimed.png' ?>" alt="Convênio Unimed" loading="lazy" >
								</div>
							</div>
							<div class="buttons-container-slides">
								<p class="text-swipe"><strong><< </strong> Deslize para ver mais. <strong> >></strong></p>
								<button class="slide-left-slides"></button>
								<button class="slide-right-slides"></button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="especialidades">
		<!-- horizontal bar -->
		<div class="detail-column special">				
		</div>
		<!-- end -->
		<div class="container">
			<div class="col border">
				<h3 class="subtitle-main light">Conheça nossas Principais <p class="title-main light">Especialidades</p></h3>
				<span class="line-purple light-line"></span>
				<p class="desc-special light">O Centro Médico Alphaville oferece as principais especialidades com os melhores profissionais de saúde.</p>
				<div class="slider-buttons">
					<button class="slide-left">
						<img src="<?php echo get_template_directory_uri(). '/img/icons/icon-arrow-slider-special.png' ?>" loading="lazy">
					</button>
					<button class="slide-right">
						<img src="<?php echo get_template_directory_uri(). '/img/icons/icon-arrow-slider-special.png' ?>" loading="lazy">
					</button>
				</div>
			</div>
			<div class="special-items">
				<div class="col-sm item-box">
					<a href="<?php echo get_permalink(139); ?>">
					<div class="special-item">
						<span class="bar-title"></span>
						<img class="img-aside" src="<?php echo get_template_directory_uri(). '/img/especialidades/ginecologia.jpg' ?>" alt="Ginecologia" loading="lazy">
						<div class="text-box">
							<p class="title-item">Ginecologia</p>
							<p>O ginecologista é o responsável por tratar problemas como cólicas menstruais, doenças sexualmente transmissíveis, endometriose e outras. </p>
						</div>
					</div>
					</a>
				</div>
				<div class="col-sm item-box">
						<a href="<?php echo get_permalink(150); ?>">
					<div class="special-item">
						<span class="bar-title"></span>
						<img class="img-aside" src="<?php echo get_template_directory_uri(). '/img/especialidades/obstetricia.jpg' ?>" alt="Obstetrícia" loading="lazy">
						<div class="text-box">
							<p class="title-item">Obstetrícia</p>
							<p>O obstetra é o médico especialista em acompanhar o desenvolvimento do feto, além de prestar assistência à mulher, desde o pré-natal até o parto e o pós-parto.</p>
						</div>
					</div>
					</a>
				</div>
				<div class="col-sm item-box">
						<a href="<?php echo get_permalink(151); ?>">
					<div class="special-item">
						<span class="bar-title"></span>
						<img class="img-aside" src="<?php echo get_template_directory_uri(). '/img/especialidades/cardiologia.jpg' ?>" alt="Cardiologia" loading="lazy">
						<div class="text-box">
							<p class="title-item">Cardiologia</p>
							<p>A cardiologia é considerada como uma das especializações da área da medicina mais evoluídas, por conta dos grandes investimentos e tecnologias envolvidas. </p>
						</div>
					</div>
				</a>
				</div>
				<div class="col-sm item-box">
						<a href="<?php echo get_permalink(152); ?>">
					<div class="special-item">
						<span class="bar-title"></span>
						<img class="img-aside" src="<?php echo get_template_directory_uri(). '/img/especialidades/clinica-medica.jpg' ?>" alt="Clínica Médica" loading="lazy">
						<div class="text-box">
							<p class="title-item">Clínica Médica</p>
							<p>A Clínica Médica é uma área de especialização da Medicina que cuida da prevenção, diagnóstico e tratamento clínico de doenças.</p>
						</div>
					</div>
					</a>
				</div>
				<div class="col-sm item-box">
						<a href="<?php echo get_permalink(153); ?>">
					<div class="special-item">
						<span class="bar-title"></span>
						<img class="img-aside" src="<?php echo get_template_directory_uri(). '/img/especialidades/medicina-fetal.jpg' ?>" alt="Medicina Fetal" loading="lazy">
						<div class="text-box">
							<p class="title-item">Medicina Fetal</p>
							<p>A Medicina Fetal é considerada uma subespecialidade da Obstetrícia dedicada a dar uma maior assistência ao feto, acompanhando todo o desenvolvimento fetal. </p>
						</div>
					</div>
				</a>
				</div>
				<div class="col-sm item-box">
						<a href="<?php echo get_permalink(154); ?>">
					<div class="special-item">
						<span class="bar-title"></span>
						<img class="img-aside" src="<?php echo get_template_directory_uri(). '/img/especialidades/ecocardiograma-fetal.jpg' ?>" alt="Ecocardiograma Fetal" loading="lazy">
						<div class="text-box">
							<p class="title-item">Ecocardiograma Fetal</p>
							<p>O Ecocardiograma Fetal é um exame responsável por avaliar, em tempo real, algumas características do coração do feto durante a gravidez.</p>
						</div>
					</div>
				</a>
				</div>
				<div class="col-sm item-box">
						<a href="<?php echo get_permalink(155); ?>">
					<div class="special-item">
						<span class="bar-title"></span>
						<img class="img-aside" src="<?php echo get_template_directory_uri(). '/img/especialidades/cardiotocografia.jpg' ?>" alt="Cardiotocografia" loading="lazy">
						<div class="text-box">
							<p class="title-item">Cardiotocografia</p>
							<p>A cardiotocografia (CTG) é um exame realizado durante a gravidez para avaliar o bem-estar fetal. Esse exame pode ser indicado após as 37 semanas de gestação.</p>
						</div>
					</div>
				</a>
				</div>
				<div class="col-sm item-box">
						<a href="<?php echo get_permalink(156); ?>">
					<div class="special-item">
						<span class="bar-title"></span>
						<img class="img-aside" src="<?php echo get_template_directory_uri(). '/img/especialidades/eletrocardiograma.jpg' ?>" alt="Eletrocardiograma" loading="lazy">
						<div class="text-box">
							<p class="title-item">Eletrocardiograma</p>
							<p>O eletrocardiograma é um exame que observa o ritmo, a velocidade e a quantidade das batidas do coração.</p>
						</div>
					</div>
				</a>
				</div>
				<div class="col-sm item-box">
						<a href="<?php echo get_permalink(157); ?>">
					<div class="special-item">
						<span class="bar-title"></span>
						<img class="img-aside" src="<?php echo get_template_directory_uri(). '/img/especialidades/uroginecologia.jpg' ?>" alt="Uroginecologia" loading="lazy">
						<div class="text-box">
							<p class="title-item">Uroginecologia</p>
							<p>A uroginecologia é uma subespecialidade da ginecologia e da urologia que abrange o tratamento do sistema urinário feminino e suas estruturas adjacentes.</p>
						</div> 
					</div>
				</a>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_template_part('includes/components/form-ajuda'); ?>

<section id="centro-medico">
	<!-- horizontal bar -->
	<div class="detail-column center-medic">
	</div>
	<!-- end -->
	<div class="container">
		<div class="align-title">
			<h4 class="title-invert">Sobre o<p class="subtitle-invert">Centro Médico Alphaville</p></h4>
			<span class="line-purple"></span>

			<p class="desc-main desc-side">A combinação de uma nova instalação no centro de Alphaville no Edifício Alpha Premium, médicos especializados por instituições de ponta, serviços em medicina diagnóstica, somadas aos 24 anos de experiência da clínica, garantem que qualquer atendimento no Centro Médico Alphaville corresponda a sua expectativa. O propósito é oferecer exames e atendimento de excelência em todos os serviços prestados.</p>

			<p class="desc-main desc-side">Reconhecemos que cada paciente é único. Portanto, não importam quais sejam as circunstâncias ou fases da vida, nossos médicos e profissionais irão fornecer assistência integral e compassiva às suas necessidades.</p>

			<p class="desc-main desc-side">O Centro Médico Alphaville emprega tecnologia de ponta e conhecimentos sempre atualizados. Além disso, uma equipe médica formada por profissionais especialistas asseguram sempre o máximo cuidado em atenção médica.</p>
			<a href="<?php echo get_permalink(16); ?>">
				<button class="btn-purple btn-medic-center">Saiba Mais</button>
			</a>
			
			<div class="buttons-medic-center">
				<p class="text-swipe"><strong><< </strong> Deslize para ver mais. <strong> >></strong></p>
				<button class="btn-left-alphaville">
					<img src="<?php echo get_template_directory_uri(). '/img/icons/icon-arrow-slider-special.png' ?>" alt="">
				</button>
				<button class="btn-right-alphaville">
					<img src="<?php echo get_template_directory_uri(). '/img/icons/icon-arrow-slider-special.png' ?>" alt="">
				</button>

			</div>

			<div class="slider-medic-center">
				<div class="aside-slider">
					<img src="<?php echo get_template_directory_uri(). '/img/center-about.jpg' ?>">
				</div>
				<div class="aside-slider">
					<img src="<?php echo get_template_directory_uri(). '/img/center-about.jpg' ?>">
				</div>
				<div class="aside-slider">
					<img src="<?php echo get_template_directory_uri(). '/img/center-about.jpg' ?>">
				</div>
				<div class="aside-slider">
					<img src="<?php echo get_template_directory_uri(). '/img/center-about.jpg' ?>">
				</div>
				<div class="aside-slider">
					<img src="<?php echo get_template_directory_uri(). '/img/center-about.jpg' ?>">
				</div>

			</div>
		</div>
	</div>

</div>
</section>
<section id="missao">
	<div class="bg-missao">
		<div class="container">
			<p class="bg-text">missão</p>
			<p class="missao-text">Oferecer o melhor da medicina moderna em todos os serviços prestados, sempre compreendendo a necessidade e individualidade de cada caso.</p>
		</div>
	</div>
</section>

<?php get_template_part('includes/components/contact'); ?>

</main>
<?php get_footer(); ?>