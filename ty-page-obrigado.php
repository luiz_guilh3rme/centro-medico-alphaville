<?php
//Template Name: Typage-Template
get_header();
?>

<section id="page-obrigado">
	<div class="top-main-title-align ty-page">
		<div class="container">
			<div class="content-text">
			<?php				
				// TO SHOW THE PAGE CONTENTS
				while ( have_posts() ) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
					<?php the_content(); ?> <!-- Page Content -->
				<?php
				    endwhile; //resetting the page loop
				    wp_reset_query(); //resetting the page query
				    ?>
				<p class="text-back"><a class="btn-back" href="<?php echo home_url(); ?>">Clique aqui </a>para voltar a página principal.</p>				
			</div>
		</div>
	</div>
	
</section>
<?php get_footer(); ?>
