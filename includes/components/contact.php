<section id="contact">
	<!-- horizontal bar -->
	<div class="detail-column contact">
	</div>
	<!-- end -->
	<div class="container">
		<div class="align-title">
			<h4 class="title-invert">Entre em<p class="subtitle-invert">Contato</p></h4>
			<span class="line-purple line-contact"></span>

			<p class="desc-main text-contact">Disponibilizamos canais exclusivos para atendimento de nossos pacientes. Preencha o formulário de contato abaixo.</p>
			
			<div class="form-contact">
				<form data-modal="form-contato">
					<input type="hidden" name="formulario" value="Formulário - Contato">
					<input class="input-form-contact" type="text" placeholder="Nome" required>
					<input class="input-form-contact" type="email" placeholder="E-mail" required>
					<input class="input-form-contact" type="text" name="telefone" placeholder="Telefone" minlength=14 required>
					<select class="select-assunto" name="assunto">
						<option value="">Escolha um Assunto</option>
						<option value="Especialidades">Especialidades</option>
						<option value="Convênios">Convênios</option>
						<option value="Outros">Outros</option>
					</select>
					<textarea name="mensagem" placeholder="Mensagem" id="" cols="30" rows="10"></textarea>
					
					<button class="btn-submit-contact">
						<img src="<?php echo get_template_directory_uri(). '/img/icons/icon-plane-btn.png' ?>" alt="Enviar contato" loading="lazy">
						Enviar
					</button>

				</form>
			</div>
			<div class="aside-image">
				<img src="<?php echo get_template_directory_uri(). '/img/img-casal-contact.png' ?>" alt="Casal com mulher grávida" loading="lazy">
			</div>
		</div>
	</div>
</section>