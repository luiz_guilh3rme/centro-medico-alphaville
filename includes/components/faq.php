<div id="faq">
	<div class="container">
		<div class="side-items">
		<div class="col-2">
			<div class="img-doctor">
				<img src="<?php echo get_template_directory_uri(). '/img/doctor-faq.png'; ?>" loading="lazy" alt="Doutor">
			</div>	
		</div>
		<div class="col-2">	
			<div class="questions">
				<h4 class="questions-title">Perguntas <strong>Frequentes</strong></h4>
				<?php 
				$ID = get_queried_object_id();
				if ( have_rows( 'faq', $ID ) ) :
					while ( have_rows( 'faq', $ID ) ) : 
						the_row();
						?>
						<div class="accordion-instance">
							<p class="accordion-question t-white t-r lh-xxg">
								<?php the_sub_field('question'); ?>
								<button class="open-question" aria-label="Abrir / Fechar dúvida"></button>
							</p>
							<div class="accordion-answer cms-wrap">
								<?php the_sub_field('answer') ?>
							</div>
						</div>
				
					<?php 
				endwhile; 
			endif;
			?>
			</div>
		</div>
	</div>
	</div>
</div>