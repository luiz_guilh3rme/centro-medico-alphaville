		<div class="col-1">
					<div class="box-form-help">
						<img src="<?php echo get_template_directory_uri(). '/img/img-family-form.png' ?>" alt="Nós podemos te ajudar!" loading="lazy">		
						<div class="content-box-help">
							<div class="text-help">
								<p class="title-help">Nós Podemos</p>
								<p class="subtitle-help">Te ajudar</p>
								<p class="desc-help">Preencha o formulário para entrar em contato conosco via e-mail e tire suas dúvidas ou deixe um comentário.</p>
							</div>
							<div class="form-help">
								<form data-modal="form-nos-te-ligamos">
									<input type="hidden" name="formulario" value="Formulário - Nós podemos te ajudar">
									<input class="input-help" type="text" name="nome" placeholder="Nome" required>
									<input class="input-help" type="tel" name="telefone" placeholder="Telefone" minlength=14 required >
									<input type="text" class="input-help input-small" name="data" placeholder="Data" required>
									<input type="text" class="input-help input-small" name="hora" placeholder="Hora" required>
									<textarea name="mensagem" placeholder="Mensagem" class="textarea-help" cols="30" rows="5" required></textarea>
									<button class="btn-send">
										<img class="arrow-btn-send" src="<?php echo get_template_directory_uri(). '/img/icons/icon-plane-btn.png' ?>" loading="lazy">
										Enviar
									</button>
								</form>
							</div>
						</div>
					</div>
				</div>