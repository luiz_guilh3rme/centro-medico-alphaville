<section id="form-ajuda">
	<!-- horizontal bar -->
	<div class="detail-column help">
	</div>
	<!-- end -->
	<div class="container">
		<div class="row">
			<div class="items-form">
				<h4>Nós podemos <strong>te Ajudar!</strong></h4>
				<span class="line-purple white-line"></span>
				<p>Preencha o formulário para entrar em contato conosco via e-mail e tire suas dúvidas ou deixe um comentário.</p>
				<button class="btn-show-contact">
						<img src="<?php echo get_template_directory_uri(). '/img/icons/icon-arrow-btn-contact.png' ?>" alt="Ícone do botão para solicitar contato" loading="lazy">
								Solicitar Contato
					</button>
				<div class="fields-help">
					<form data-modal="form-podemos-te-ajudar">
						<input type="hidden" name="formulario" value="Formulário - Nós podemos te ajudar!">
						<input type="text" placeholder="Nome" class="input-small" name="nome" required>
						<input type="phone" placeholder="Telefone" class="input-small" name="telefone" minlength=14 required>
						<input type="email" placeholder="Seu melhor E-mail" class="input-small" name="email" required>
						<textarea name="mensagem" placeholder="Mensagem" cols="30" rows="5"></textarea>
						<button class="btn-submit-help">
							<img src="<?php echo get_template_directory_uri(). '/img/icons/icon-plane-btn.png' ?>" loading="lazy">
							Enviar
						</button>
					</form>
				</div>
				<div class="img-form-help">
					<img class="img-help" src="<?php echo get_template_directory_uri(). '/img/img-family-form.png' ?>" alt="Imagem com desenho de uma família" loading="lazy">
				</div>
			</div>
		</div>
	</div>
</section>