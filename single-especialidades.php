<?php
//Template Name: Item-Especialidades
//Template Post Type: especialidades
get_header();
?>

<section id="page-especialidades">
	<?php $url = wp_get_attachment_image_url( get_post_thumbnail_id($post->ID), 'full-post'); ?>
	<img class="main-bg" src="<?php echo $url; ?>" alt="Nossas Especialidades" loading="lazy">
		<!-- horizontal bar -->
		<div class="detail-column box-page">	
								
		</div>
	<!-- end -->
	<div class="bg-special">
		<div class="container">
			<div class="title-post">
				<h1>Nossas<p>Especialidades</p></h1>
			</div>
		</div>
	</div>
<div class="bg-special">
	<div class="container">
		<div class="col">
			<div class="breadcrumb">
				<span class="line-purple bar-page"></span>	
				<?php get_breadcrumb(); ?>
			</div>
			<p class="desc-post">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam quis eveniet, quos, doloribus dignissimos cum corporis optio iste recusandae dolores similique aperiam ipsam voluptatum saepe, voluptate, possimus rerum molestias. Sint!</p>

			<div class="col-sm box-special">
				<div class="line-box"></div>
				<div class="special-item">
					<div class="border-image">
						<img class="img-aside" src="<?php echo get_template_directory_uri(). '/img/img-photobox.png' ?>" loading="lazy">
					</div>
					<div class="text-box">
						<div class="title-box">
							<p><strong>Ginecologia</strong></p>

						</div>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum omnis dolor tenetur, animi deleniti consectetur fugiat dolorum sequi fuga aliquid iure tempore, ut laudantium ipsa error aut placeat, voluptatum facilis.</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aliquam nulla tempora, possimus eaque dolorem ut placeat debitis tempore facere consectetur, minus id cumque repellendus dolor dolorum, incidunt neque quae.</p>
					</div>
				</div>
			</div>

			<div class="col-sm box-special">
				<div class="special-item">
					<div class="border-image">
						<img class="img-aside" src="<?php echo get_template_directory_uri(). '/img/img-photobox.png' ?>" loading="lazy">
					</div>
					<div class="text-box">
						<div class="title-box">
							<p><strong>Ginecologia</strong></p>
						</div>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum omnis dolor tenetur, animi deleniti consectetur fugiat dolorum sequi fuga aliquid iure tempore, ut laudantium ipsa error aut placeat, voluptatum facilis.</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aliquam nulla tempora, possimus eaque dolorem ut placeat debitis tempore facere consectetur, minus id cumque repellendus dolor dolorum, incidunt neque quae.</p>
					</div>
				</div>
			</div>
			<div class="col-sm box-special">
				<div class="special-item">
					<div class="border-image">
						<img class="img-aside" src="<?php echo get_template_directory_uri(). '/img/img-photobox.png' ?>" loading="lazy">
					</div>
					<div class="text-box">
						<div class="title-box">
							<p><strong>Ginecologia</strong></p>
						</div>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum omnis dolor tenetur, animi deleniti consectetur fugiat dolorum sequi fuga aliquid iure tempore, ut laudantium ipsa error aut placeat, voluptatum facilis.</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aliquam nulla tempora, possimus eaque dolorem ut placeat debitis tempore facere consectetur, minus id cumque repellendus dolor dolorum, incidunt neque quae.</p>
					</div>
				</div>
			</div>
			<div class="col-sm box-special">
				<div class="special-item">
					<div class="border-image">
						<img class="img-aside" src="<?php echo get_template_directory_uri(). '/img/img-photobox.png' ?>" loading="lazy">
					</div>
					<div class="text-box">
						<div class="title-box">
							<p><strong>Ginecologia</strong></p>
						</div>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum omnis dolor tenetur, animi deleniti consectetur fugiat dolorum sequi fuga aliquid iure tempore, ut laudantium ipsa error aut placeat, voluptatum facilis.</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aliquam nulla tempora, possimus eaque dolorem ut placeat debitis tempore facere consectetur, minus id cumque repellendus dolor dolorum, incidunt neque quae.</p>
					</div>
				</div>
			</div>
			<div class="col-sm box-special">
				<div class="special-item">
					<div class="border-image">
						<img class="img-aside" src="<?php echo get_template_directory_uri(). '/img/img-photobox.png' ?>" loading="lazy">
					</div>
					<div class="text-box">
						<div class="title-box">
							<p><strong>Ginecologia</strong></p>
						</div>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum omnis dolor tenetur, animi deleniti consectetur fugiat dolorum sequi fuga aliquid iure tempore, ut laudantium ipsa error aut placeat, voluptatum facilis.</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aliquam nulla tempora, possimus eaque dolorem ut placeat debitis tempore facere consectetur, minus id cumque repellendus dolor dolorum, incidunt neque quae.</p>
					</div>
				</div>
			</div>
			<div class="col-sm box-special">
				<div class="special-item">
					<div class="border-image">
						<img class="img-aside" src="<?php echo get_template_directory_uri(). '/img/img-photobox.png' ?>" loading="lazy">
					</div>
					<div class="text-box">
						<div class="title-box">
							<p><strong>Ginecologia</strong></p>
						</div>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum omnis dolor tenetur, animi deleniti consectetur fugiat dolorum sequi fuga aliquid iure tempore, ut laudantium ipsa error aut placeat, voluptatum facilis.</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aliquam nulla tempora, possimus eaque dolorem ut placeat debitis tempore facere consectetur, minus id cumque repellendus dolor dolorum, incidunt neque quae.</p>
					</div>
				</div>
			</div>
			<div class="col-sm box-special">
				<div class="special-item">
					<div class="border-image">
						<img class="img-aside" src="<?php echo get_template_directory_uri(). '/img/img-photobox.png' ?>" loading="lazy">
					</div>
					<div class="text-box">
						<div class="title-box">
							<p><strong>Ginecologia</strong></p>
						</div>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum omnis dolor tenetur, animi deleniti consectetur fugiat dolorum sequi fuga aliquid iure tempore, ut laudantium ipsa error aut placeat, voluptatum facilis.</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aliquam nulla tempora, possimus eaque dolorem ut placeat debitis tempore facere consectetur, minus id cumque repellendus dolor dolorum, incidunt neque quae.</p>
					</div>
				</div>
			</div>
			<div class="col-sm box-special">
				<div class="special-item">
					<div class="border-image">
						<img class="img-aside" src="<?php echo get_template_directory_uri(). '/img/img-photobox.png' ?>" loading="lazy">
					</div>
					<div class="text-box">
						<div class="title-box">
							<p><strong>Ginecologia</strong></p>
						</div>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum omnis dolor tenetur, animi deleniti consectetur fugiat dolorum sequi fuga aliquid iure tempore, ut laudantium ipsa error aut placeat, voluptatum facilis.</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aliquam nulla tempora, possimus eaque dolorem ut placeat debitis tempore facere consectetur, minus id cumque repellendus dolor dolorum, incidunt neque quae.</p>
					</div>
				</div>
			</div>
			<div class="col-sm box-special">
				<div class="special-item">
					<div class="border-image">
						<img class="img-aside" src="<?php echo get_template_directory_uri(). '/img/img-photobox.png' ?>" loading="lazy">
					</div>
					<div class="text-box">
						<div class="title-box">
							<p><strong>Ginecologia</strong></p>
						</div>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum omnis dolor tenetur, animi deleniti consectetur fugiat dolorum sequi fuga aliquid iure tempore, ut laudantium ipsa error aut placeat, voluptatum facilis.</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aliquam nulla tempora, possimus eaque dolorem ut placeat debitis tempore facere consectetur, minus id cumque repellendus dolor dolorum, incidunt neque quae.</p>
					</div>
				</div>
			</div>
		
		</div>
	</div>
</div>
	<?php get_template_part( 'includes/components/form-ajuda'); ?>
	<?php get_template_part('includes/components/contact') ?>

</section>
<?php get_footer(); ?>