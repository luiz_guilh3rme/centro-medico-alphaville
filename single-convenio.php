<?php
//Template Post Type: Convenio
//Template Name: Template-Tipo-Convênio
get_header();
?>

<section id="page-convenio">
	<div class="top-main-title-align">
		<?php 
		if(wp_is_mobile()){
			$url = wp_get_attachment_image_url( the_post_thumbnail(), 'mobile-post'); 
		}else{
			$url = wp_get_attachment_image_url( the_post_thumbnail(), 'full-post');
		}
		?>
		<!-- horizontal bar -->
		<div class="detail-column box-page"></div>
		<!-- end -->
		<div class="container">
			<div class="title-post">
				<h1><?php the_title(); ?></p></h1>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="col-3 left desc-post-full">
			<div class="breadcrumb">
				<span class="line-purple bar-page"></span>	
				<?php get_breadcrumb(); ?>
			</div>
			<div class="content">
			<?php
			// TO SHOW THE PAGE CONTENTS
			while ( have_posts() ) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
			<?php the_content(); ?> <!-- Page Content -->
			<?php
			    endwhile; //resetting the page loop
			    wp_reset_query(); //resetting the page query
			    ?>
			    </div>
			</div>
			
			
			<?php 
			if(wp_is_mobile()){
				get_template_part('includes/components/form-ajuda'); 
			}else{

				get_template_part('includes/components/form-aside-ajuda');
			
			}	
			 ?>
			


		</div>
	
	</section>
	<div class="col">
		<?php get_template_part('includes/components/contact') ?>
	</div>
	<?php get_footer(); ?>
