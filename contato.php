<?php
//Template Name: Template-Contato
get_header();
?>

<section id="page-contato">
	<div class="top-main-title-align">
		<?php 
		if(wp_is_mobile()){
			$url = wp_get_attachment_image_url( the_post_thumbnail(), 'mobile-post'); 
		}else{
			$url = wp_get_attachment_image_url( the_post_thumbnail(), 'full-post');
		}
		?>
		<div class="container">
			<div class="title-post">
				<h1><p>Localização</p></h1>
			</div>
		</div>
	</div>
	<div class="bg-special">
		<div class="container">
			<div class="col">
				<div class="breadcrumb">
					<span class="line-purple bar-page"></span>	
					<?php get_breadcrumb(); ?>
				</div>
				<?php
	    				// TO SHOW THE PAGE CONTENTS
				while ( have_posts() ) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
				<div class="desc-post desc-sobre txt-contato">
					<?php the_content(); ?> <!-- Page Content -->

					<?php
				    endwhile; //resetting the page loop
				    wp_reset_query(); //resetting the page query
				    ?>
				</div>
				<div class="contacts">
					<div class="contact-phone">
						<p class="big-icon">
							<i class="fas fa-phone-alt"></i> :
						</p>
						<p class="numbers-phone">
							<a href="tel:+551141916041">11 4191.6041</a> | 
							<a href="tel:+551141935748">11 4193.5748</a> | 
							<a href="tel:+551141935741">11 4193.5741</a>
						</p>
					</div>

					<div class="contact-whats">
						<p class="big-icon whats-icon-contact">
							<i class="fab fa-whatsapp"></i> :
						</p>
						<p class="numbers-phone">
							<a href="#">11 95987.1400</a> | 
							<a href="#">11 96457.0605</a> 
						</p>
					</div>
				</div>
				<div class="address">
					<p class="txtaddress">
						<img src="<?php echo get_template_directory_uri(). '/img/icons/icon-local.png' ?>" loading="lazy">
						Endereço: 
					</p>
					<p class="desc-address">
						Al. Rio Negro, <strong>967</strong> - Ed. Alpha Premium - Sala <strong>503 | Alphaville | Barueri - SP |</strong> CEP: <strong>06465-000</strong>
					</p>
					<p class="txtaddress mail">
						<img src="<?php echo get_template_directory_uri(). '/img/icons/icon-mail.png' ?>" loading="lazy">
						E-mail:
					</p>
					<p class="desc-address mail-txt">
						atendimento@<strong>centromedicoalphaville</strong>.com
					</p>
				</div>
			</div>
			<div class="col-1 aside">
				<div class="aside-image-contact">
					<img src="<?php echo get_template_directory_uri(). '/img/img-contact.jpg' ?>" alt="Localização" loading="lazy">		
				</div>
			</div>
		</div>
		<div class="col">
			<div class="map">
				<div class="mapouter">
					<div class="gmap_canvas">
						<iframe width="100%" height="500px" id="gmap_canvas" src="https://maps.google.com/maps?q=Alameda%20Rio%20Negro%2C%20967&t=&z=17&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php get_footer(); ?>
