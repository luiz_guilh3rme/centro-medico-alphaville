<!DOCTYPE html>
<!--[if IE]><html lang="pt-br" class="lt-ie9 lt-ie8"><![endif]-->
<html lang="pt-br">
<?php get_template_part('includes/components/head'); ?>
<header>
	<div class="bg-first"></div>
	<div class="top-items">
		<div class="grid-row">
			<!-- Top items -->
			<div class="top-infos">
				<div class="top-header">
					<img class="icon-local" src="<?php echo get_template_directory_uri(). '/img/icons/icon-local.png'; ?>" alt="Localização" loading="lazy">
					<p class="bold">Al. Rio Negro, 967 - Edf. Alpha Premium - Sala 503 </p>
					<p>| Alphaville | Barueri-SP | 06454-000</p>
					<img src="<?php echo get_template_directory_uri() . '/img/icons/icon-phone.png'; ?>" alt="Telefones de Contato" loading="lazy">
					<p class="bold">
						<a href="tel:+551141916041">11 4191.6041</a> / <a href="tel:+551141935748">4193.5748</a> / <a href="tel:+551141935741">4193.5741</a>
					</p>
					<img src="<?php echo get_template_directory_uri(). '/img/icons/icon-whats.png'; ?>" alt="Whatsapp" loading="lazy"> 
					<p class="bold">
						<a href="">11 95987.1400</a> / <a href="">96457.0605</a>
					</p>  
					<img src="<?php echo get_template_directory_uri(). '/img/icons/icon-mail.png' ?>" alt="E-mail" loading="lazy">        
					<p class="bold"><a href="mailto:atendimento@centromedicoalphaville.com">atendimento@centromedicoalphaville.com</a></p>
				</div>
				<a href="#" target="_blank"><img class="face-logo" src="<?php echo get_template_directory_uri(). '/img/icons/logo-facebook.png' ?>" alt="Facebook" loading="lazy"></a>
			</div>

		</div>
		<!-- Main menu -->
		<div class="menu-top">
			<div class="container">
				<a class="logo-menu" href="<?php echo home_url(); ?>">
				</a>
				<div class="main-menu">

					<div class="icon-menu-mobile">
					</div>
					<div class="icon-close-mobile">
						
					</div>
					<div class="arrow-menu"></div>
					<?php
					wp_nav_menu([
						'theme_location' => 'header-menu-home',
						'menu_class' => 'items-menu',
						'container_class' => 'main-nav',
					]);
					?>
				</div>
			</div>
		</div>
		<!--  Main menu - mobile -->
		<div class="outside-menu"></div>
		<div class="cover-menu">
			<div class="mobile-menu">
				<?php 
				wp_nav_menu([
					'theme_location' => 'header-menu-home-mobile',
					'menu_class' => 'items-menu-mobile',
					'container_class' => 'main-nav-mobile',
				]);
				?>
			</div>
		</div>
	</div>
</header>

<body <?php body_class(); ?>>
	
