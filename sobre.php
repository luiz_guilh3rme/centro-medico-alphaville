<?php
//Template Name: Template-Sobre
get_header();
?>

<section id="page-sobre">
	<div class="top-main-title">
		<?php $url = wp_get_attachment_image_url( the_post_thumbnail(), 'full-post'); ?>
		<div class="container">
			<div class="title-post">
				<h1>Sobre o <p>Centro Médico Alphaville</p></h1>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="col-3 left desc-full">
			<div class="breadcrumb">
				<span class="line-purple bar-page"></span>	
				<?php get_breadcrumb(); ?>
			</div>
			
			<?php
			// TO SHOW THE PAGE CONTENTS
			while ( have_posts() ) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->

			<?php the_content(); ?> <!-- Page Content -->

			<?php
				    endwhile; //resetting the page loop
				    wp_reset_query(); //resetting the page query
				    ?>
				    <div class="images-thumbs">

				    	<div class="box-image">
				    		<a class="img-gallery" data-fancybox="gallery" href="<?php echo get_template_directory_uri(). '/img/sobre/01.jpg'; ?>">
				    			<img src="<?php echo get_template_directory_uri(). '/img/sobre/thumbs/01.jpg'; ?>">
				    			<div class="outline-image"></div>
				    		</a>
				    	</div>
				    	<div class="box-image">
				    		<a class="img-gallery" data-fancybox="gallery" href="<?php echo get_template_directory_uri(). '/img/sobre/02.jpg'; ?>">
				    			<img src="<?php echo get_template_directory_uri(). '/img/sobre/thumbs/02.jpg'; ?>">
				    			<div class="outline-image"></div>
				    		</a>
				    	</div>
				    	<div class="box-image">
				    		<a class="img-gallery" data-fancybox="gallery" href="<?php echo get_template_directory_uri(). '/img/sobre/03.jpg'; ?>">
				    			<img src="<?php echo get_template_directory_uri(). '/img/sobre/thumbs/03.jpg'; ?>">
				    			<div class="outline-image"></div>
				    		</a>
				    	</div>

				    	<div class="box-image">
				    		<a class="img-gallery" data-fancybox="gallery" href="<?php echo get_template_directory_uri(). '/img/sobre/04.jpg'; ?>">
				    			<img src="<?php echo get_template_directory_uri(). '/img/sobre/thumbs/04.jpg'; ?>">
				    			<div class="outline-image"></div>
				    		</a>
				    	</div>
				    	<div class="box-image">
				    		<a class="img-gallery" data-fancybox="gallery" href="<?php echo get_template_directory_uri(). '/img/sobre/05.jpg'; ?>">
				    			<img src="<?php echo get_template_directory_uri(). '/img/sobre/thumbs/05.jpg'; ?>">
				    			<div class="outline-image"></div>
				    		</a>
				    	</div>
				    	<div class="box-image">
				    		<a class="img-gallery" data-fancybox="gallery" href="<?php echo get_template_directory_uri(). '/img/sobre/06.jpg'; ?>">
				    			<img src="<?php echo get_template_directory_uri(). '/img/sobre/thumbs/06.jpg'; ?>">
				    			<div class="outline-image"></div>
				    		</a>
				    	</div>
				    	<div class="box-image">
				    		<a class="img-gallery" data-fancybox="gallery" href="<?php echo get_template_directory_uri(). '/img/sobre/07.jpg'; ?>">
				    			<img src="<?php echo get_template_directory_uri(). '/img/sobre/thumbs/07.jpg'; ?>">
				    			<div class="outline-image"></div>
				    		</a>
				    	</div>

				    	<div class="box-image">
				    		<a class="img-gallery" data-fancybox="gallery" href="<?php echo get_template_directory_uri(). '/img/sobre/08.jpg'; ?>">
				    			<img src="<?php echo get_template_directory_uri(). '/img/sobre/thumbs/08.jpg'; ?>">
				    			<div class="outline-image"></div>
				    		</a>
				    	</div>
				    	<div class="box-image">
				    		<a class="img-gallery" data-fancybox="gallery" href="<?php echo get_template_directory_uri(). '/img/sobre/09.jpg'; ?>">
				    			<img src="<?php echo get_template_directory_uri(). '/img/sobre/thumbs/09.jpg'; ?>">
				    			<div class="outline-image"></div>
				    		</a>
				    	</div>
				    	<div class="box-image">
				    		<a class="img-gallery" data-fancybox="gallery" href="<?php echo get_template_directory_uri(). '/img/sobre/10.jpg'; ?>">
				    			<img src="<?php echo get_template_directory_uri(). '/img/sobre/thumbs/10.jpg'; ?>">
				    			<div class="outline-image"></div>
				    		</a>
				    	</div>
				    	<div class="box-image">
				    		<a class="img-gallery" data-fancybox="gallery" href="<?php echo get_template_directory_uri(). '/img/sobre/11.jpg'; ?>">
				    			<img src="<?php echo get_template_directory_uri(). '/img/sobre/thumbs/11.jpg'; ?>">
				    			<div class="outline-image"></div>
				    		</a>
				    	</div>
				    	<div class="box-image">
				    		<a class="img-gallery" data-fancybox="gallery" href="<?php echo get_template_directory_uri(). '/img/sobre/12.jpg'; ?>">
				    			<img src="<?php echo get_template_directory_uri(). '/img/sobre/thumbs/12.jpg'; ?>">
				    			<div class="outline-image"></div>
				    		</a>
				    	</div>
				    </div>
				</div>
				<p class="text-swipe text-gallery"><strong><< </strong>Deslize para ver mais.<strong> >></strong></p>
				<?php 
				if(wp_is_mobile()){
					get_template_part('includes/components/form-ajuda'); 
				}else{

					get_template_part('includes/components/form-aside-ajuda');
					
				}	
				?>
			</div>
			<div class="container">
				<div class="align-title">
					<h2 class="subtitle-invert">Diferenciais</h2>
					<span class="line-purple"></span>
					<div class="content-text">
						<p class="desc-default text-about">Inaugurada há 24 anos, a Centro Médico Alphaville investe no atendimento humanizado e de qualidade, oferecendo também o máximo possível em conforto e facilidade para seus pacientes.</p>

<p class="desc-default text-about">A clínica conta com um corpo clínico formado por médicos experientes e especializados por instituições de renome, como Instituto Dante Pazzanese de Cardiologia, USP, FAMERP e PUC-SP e PUC-Sorocaba.</p>

<p class="desc-default text-about">Além de fazerem parte do corpo clínico dos principais hospitais de São Paulo, a clínica oferece:</p>

<ul>
<li>Fácil acesso a localização na Al. Rio Negro;</li>
<li>Estacionamento (Alpha Premium) com 20% de desconto para os pacientes;</li>
<li>Sala de espera ampla e climatizada.</li>
</ul>
	

						
						
						
					</div>
				</div>
			</div>
		</section>
		<section id="equipe">
			<!-- horizontal bar -->
			<div class="detail-column box-equipe"></div>
			<!-- end -->
			<div class="container">
				<div class="align-title items-team">
					<h3 class="subtitle-invert white">Equipe</h3>
					<span class="line-purple"></span>
					<div class="content-text">
						<p class="desc-default text-about white">Nossa equipe é completa nos serviços prestados e é formada por médicos experientes especializados por instituições de renome como Instituto Dante Pazzanese de Cardiologia, USP, FAMERP e PUC-SP-Sorocaba. Fazemos parte do corpo clínico dos principais Hospitais de São Paulo.</p>
					</div>

					<div class="boxes-team">
						<div class="col-sm box-team">
							<div class="team-item">
								<div class="border-team-image">
									<img class="img-aside" src="<?php echo get_template_directory_uri(). '/img/doctormale.jpg' ?>">
								</div>
								<div class="team-box-text">
									<div class="title-team-box">
										<p class="name-doctor">Dr. <strong>Pedro Ivan de Almeida Pretti</strong></p>
										<p class="crm-doctor">CRM: 149.789</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm box-team">
							<div class="team-item">
								<div class="border-team-image">
									<img class="img-aside" src="<?php echo get_template_directory_uri(). '/img/doctormale.jpg' ?>">
								</div>
								<div class="team-box-text">
									<div class="title-team-box">
										<p class="name-doctor">Dr. <strong>Wagner Pretti</strong></p>
										<p class="crm-doctor">CRM: 46.305</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm box-team">
							<div class="team-item">
								<div class="border-team-image">
									<img class="img-aside" src="<?php echo get_template_directory_uri(). '/img/doctor.jpg' ?>">
								</div>
								<div class="team-box-text">
									<div class="title-team-box">
										<p class="name-doctor">Dra. <strong>Maria Tereza Fantozi Giorgetti</strong></p>
										<p class="crm-doctor">CRM: 92.981</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm box-team">
							<div class="team-item">
								<div class="border-team-image">
									<img class="img-aside" src="<?php echo get_template_directory_uri(). '/img/doctor.jpg' ?>">
								</div>
								<div class="team-box-text">
									<div class="title-team-box">
										<p class="name-doctor">Dra. <strong>Barbara Regina Borrego</strong></p>
										<p class="crm-doctor">CRM: 48.769</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm box-team">
							<div class="team-item">
								<div class="border-team-image">
									<img class="img-aside" src="<?php echo get_template_directory_uri(). '/img/doctor.jpg' ?>">
								</div>
								<div class="team-box-text">
									<div class="title-team-box">
										<p class="name-doctor">Dra. <strong>Carla Natrielli</strong></p>
										<p class="crm-doctor">CRM: 76.747</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm box-team">
							<div class="team-item">
								<div class="border-team-image">
									<img class="img-aside" src="<?php echo get_template_directory_uri(). '/img/doctor.jpg' ?>">
								</div>
								<div class="team-box-text">
									<div class="title-team-box">
										<p class="name-doctor">Dr. <strong>Julia Yoshiko Maruki</strong></p>
										<p class="crm-doctor">CRM: 74.020</p>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</section>
		<section id="medico-associacao">
			<div class="row">
				<!-- horizontal bar -->
				<div class="detail-column sub">					
				</div>
				<!-- end -->
				<div class="container">
					<div class="col">
						<div class="sub-text text-associacao">
							<h4 class="subtitle-main text-inline">Associações <strong>médicas</strong></h4>
							<span class="line-purple line-white"></span>
							<p class="desc-full">As Associações Médicas são criadas para a promoção de troca experiência e de conhecimentos científicos, além de garantir o bom relacionamento entre médicos, clínicas, convênios e seguros de assistência médica.</p>

							<div class="container-slides">
								<div class="item-slide">
									<img src="<?php echo get_template_directory_uri(). '/img/convenios/amil.png' ?>" alt="Convênio Amil" >
								</div>
								<div class="item-slide">
									<img src="<?php echo get_template_directory_uri(). '/img/convenios/bradesco.png' ?>" alt="Convênio Bradesco" >
								</div>

								<div class="item-slide">
									<img src="<?php echo get_template_directory_uri(). '/img/convenios/cassi.png' ?>" alt="Convênio Cassi" >
								</div>

								<div class="item-slide">
									<img src="<?php echo get_template_directory_uri(). '/img/convenios/gama.png' ?>" alt="Convênio Gama Saúde" >
								</div>
								
								<div class="item-slide">
									<img src="<?php echo get_template_directory_uri(). '/img/convenios/golden-cross.png' ?>" alt="Convênio Golden Cross" >
								</div>

								<div class="item-slide">
									<img src="<?php echo get_template_directory_uri(). '/img/convenios/mapfre.png' ?>" alt="Convênio Mapfre" >
								</div>

								<div class="item-slide">
									<img src="<?php echo get_template_directory_uri(). '/img/convenios/maritima.png' ?>" alt="Convênio Marítima" >
								</div>

								<div class="item-slide">
									<img src="<?php echo get_template_directory_uri(). '/img/convenios/medial.png' ?>" alt="Convênio Medial" >
								</div>

								<div class="item-slide">
									<img src="<?php echo get_template_directory_uri(). '/img/convenios/mediservice.png' ?>" alt="Convênio Service" >
								</div>

								<div class="item-slide">
									<img src="<?php echo get_template_directory_uri(). '/img/convenios/omint.png' ?>" alt="Convênio Omint" >
								</div>
								<div class="item-slide">
									<img src="<?php echo get_template_directory_uri(). '/img/convenios/sulamerica.png' ?>" alt="Convênio Sul América" >
								</div>
								<div class="item-slide">
									<img src="<?php echo get_template_directory_uri(). '/img/convenios/unimed.png' ?>" alt="Convênio Unimed" >
								</div>
							</div>
							<div class="buttons-container-slides">
								<p class="text-swipe"><strong><< </strong>Deslize para ver mais.<strong> >></strong></p>
								<button class="slide-left-slides"></button>
								<button class="slide-right-slides"></button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<div class="col">
			<?php get_template_part('includes/components/contact') ?>
		</div>
		<?php get_footer(); ?>
