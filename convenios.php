<?php
//Template Name: Template-Convênios
get_header();
?>

<section id="page-convenios">
	<div class="top-main-title">
	<?php 
		if(wp_is_mobile()){
			$url = wp_get_attachment_image_url( the_post_thumbnail(), 'mobile-post'); 
		}else{
			$url = wp_get_attachment_image_url( the_post_thumbnail(), 'full-post');
		}
		?>

	<div class="container">
		<div class="title-post">
			<h1><p>Convênios</p></h1>
		</div>
	</div>
</div>
	<div class="container">
		<div class="col">
			<div class="breadcrumb">
				<span class="line-purple bar-page"></span>	
				<?php get_breadcrumb(); ?>
			</div>
			<div class="content-convenios">

	<?php
			// TO SHOW THE PAGE CONTENTS
			while ( have_posts() ) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
			<?php the_content(); ?> <!-- Page Content -->
			<?php
			    endwhile; //resetting the page loop
			    wp_reset_query(); //resetting the page query
			    ?>

				<div class="box-images">
					<div class="col-1">
						<a href="#">
							<img src="<?php echo get_template_directory_uri(). '/img/convenios/page/amil.png' ?>" alt="Amil" loading="lazy">
						</a>
					</div>
					<div class="col-1">
						<a href="#">
							<img src="<?php echo get_template_directory_uri(). '/img/convenios/page/bradesco.png' ?>" alt="Bradesco Saúde" loading="lazy">
						</a>
					</div>
					<div class="col-1">
						<a href="#">
							<img src="<?php echo get_template_directory_uri(). '/img/convenios/page/cassi.png' ?>" alt="Cassi" loading="lazy">
						</a>
					</div>
					<div class="col-1">
						<a href="#">
							<img src="<?php echo get_template_directory_uri(). '/img/convenios/page/gama.png' ?>" alt="Gama Saúde" loading="lazy">
						</a>
					</div>
					<div class="col-1">
						<a href="#">
							<img src="<?php echo get_template_directory_uri(). '/img/convenios/page/goldencross.png' ?>" alt="GoldenCross" loading="lazy">
						</a>
					</div>
					<div class="col-1">
						<a href="#">
							<img src="<?php echo get_template_directory_uri(). '/img/convenios/page/mapfre.png' ?>" alt="Mapfre" loading="lazy">
						</a>
					</div>
					<div class="col-1">
						<a href="#">
							<img src="<?php echo get_template_directory_uri(). '/img/convenios/page/maritima.png' ?>" alt="Marítima Saúde" loading="lazy">
						</a>
					</div>
					<div class="col-1">
						<a href="#">
							<img src="<?php echo get_template_directory_uri(). '/img/convenios/page/medial.png' ?>" alt="Medial" loading="lazy">
						</a>
					</div>
					<div class="col-1">
						<a href="#">
							<img src="<?php echo get_template_directory_uri(). '/img/convenios/page/medservice.png' ?>" alt="Medservice" loading="lazy">
						</a>
					</div>
					<div class="col-1">
						<a href="#">
							<img src="<?php echo get_template_directory_uri(). '/img/convenios/page/omint.png' ?>" alt="Omint" loading="lazy">
						</a>
					</div>
					<div class="col-1">
						<a href="#">
							<img src="<?php echo get_template_directory_uri(). '/img/convenios/page/sulamerica.png' ?>" alt="SulAmérica" loading="lazy">
						</a>
					</div>
					<div class="col-1">
						<a href="#">
							<img src="<?php echo get_template_directory_uri(). '/img/convenios/page/unimed.png' ?>" alt="Unimed" loading="lazy">
						</a>
					</div>
					
				</div>
				<div class="show-more">
					<p class="text-swipe"><strong><<</strong> Deslize para ver mais. <strong>>></strong></p>
					<button class="btn-ver-mais">Ver mais</button>
				</div>
			</div>
		</div>
	</div>

	<?php get_template_part( 'includes/components/form-ajuda'); ?>

	<div class="container">
		<div class="desc-convenios">
			<h2 class="subtitle-text">Conveniados</h2>
			<p class="desc-post">Para trazer o melhor atendimento e atenção médica possível, o <strong>Centro Médico Alphaville</strong> atende os seguintes convênios: Amil, Bradesco Saúde, Cassi, Gama Saúde, Golden Cross, Mapfre, Marítima Saúde, Medial Saúde, Mediserve, Omint, SulAmérica Saúde e Unimed.</p>

<p class="desc-post">Com a ajuda dos principais convênios do Brasil e junto com os nossos 24 anos de experiência clínica, estamos preparados para atender você e sua família em qualquer fase da vida, com médicos especialistas que trarão assistência integral de acordo com as suas necessidades.</p>
		</div>
	</div>

	<?php get_template_part('includes/components/contact') ?>

</section>
<?php get_footer(); ?>